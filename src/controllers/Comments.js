import Comment from "../models/CommentModel.js";
import db from "../config/db.js";
import { v4 as uuidv4 } from "uuid";

export const getComments = async (req, res) => {
  try {
    const comments = await db.query(
      "SELECT * FROM comment a, task b, karyawan c WHERE a.kode_task=b.kode_task AND a.no_karyawan=c.no_karyawan"
    );
    res.json({ statusCode: 200, message: "success", data: comments[0] });
  } catch (error) {
    console.log(error);
  }
};

export const getCommentByKodeTask = async (req, res) => {
  try {
    const kode_task = req.params.kode_task;
    const comments = await db.query(
      `SELECT * FROM comment a, task b, karyawan c WHERE a.kode_task='${kode_task}' AND a.kode_task=b.kode_task AND a.no_karyawan=c.no_karyawan`
    );
    res.json({ statusCode: 200, message: "success", data: comments[0] });
  } catch (error) {
    console.log(error);
  }
};

export const submitComment = async (req, res) => {
  const { kode_task, no_karyawan, deskripsi } = req.body;

  try {
    await Comment.create({
      kode_comment: uuidv4(),
      kode_task,
      no_karyawan,
      deskripsi,
    });
    res.json({ statusCode: 201, message: "Comment terkirim" });
  } catch (error) {
    console.log(error);
  }
};
