import User from "../models/UserModel.js";
import jwt from "jsonwebtoken";

export const refreshToken = async (req, res) => {
  try {
    const refToken = req.cookies.refreshToken;
    if (!refToken) return res.sendStatus(401);

    const user = User.findAll({
      where: {
        refresh_token: refToken,
      },
    });

    if (!user[0]) return res.sendStatus(403);

    jwt.verify(refToken, process.env.REFRESH_TOKEN, (err, decoded) => {
      if (err) return res.sendStatus(403);

      const userID = user[0].kode_user;
      const username = user[0].username;
      const noKaryawan = user[0].no_karyawan;
      const level = user[0].level;

      const accessToken = jwt.sign(
        { userID, username, noKaryawan, level },
        process.env.ACCESS_TOKEN,
        {
          expiresIn: "15s",
        }
      );

      res.json({ accessToken });
    });
  } catch (error) {
    console.log(error);
  }
};
