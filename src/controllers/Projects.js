import Project from "../models/ProjectModel.js";
import Tasks from "../models/TaskModel.js";
import { v4 as uuidv4 } from "uuid";

export const getProjects = async (req, res) => {
  try {
    const project = await Project.findAll();
    res.json({ statusCode: 200, message: "success", data: project });
  } catch (error) {
    console.log(error);
  }
};

export const getProjectForReport = async (req, res) => {
  try {
    let data = [];
    const task = await Tasks.findAll();
    const project = await Project.findAll();

    if (project) {
      project.forEach((val) => {
        const tempData = task.filter(
          (item) => item.kode_project === val.kode_project
        );

        data.push({
          assign: val.assign,
          balance: val.balance,
          kode_project: val.kode_project,
          nama_project: val.nama_project,
          team_leader: val.team_leader,
          lokasi: val.lokasi,
          task: tempData,
          createdAt: val.createdAt,
          updatedAt: val.updatedAt,
        });
      });

      if (data.length > 0) {
        return res.json({ statusCode: 200, message: "success", data: data });
      }
    }
    return res.json({ statusCode: 200, message: "success", data: [] });
  } catch (error) {
    console.log(error);
  }
};

export const getProjectByID = async (req, res) => {
  try {
    const kode_project = req.params.kode_project;
    const project = await Project.findOne({
      where: { kode_project },
    });

    if (!project) {
      return res.json({ statusCode: 404, message: "Data not found!" });
    }

    res.json({ statusCode: 200, message: "success", data: project });
  } catch (error) {
    console.log(error);
  }
};

export const saveProject = async (req, res) => {
  const { nama_project, balance, team_leader, assign, lokasi } = req.body;

  try {
    await Project.create({
      kode_project: uuidv4(),
      nama_project,
      balance,
      team_leader,
      assign,
      lokasi,
    });
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const updateProject = async (req, res) => {
  const { kode_project, nama_project, balance, team_leader, assign, lokasi } =
    req.body;

  try {
    await Project.update(
      {
        nama_project,
        balance,
        team_leader,
        assign,
        lokasi,
      },
      {
        where: {
          kode_project,
        },
      }
    );
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteProject = async (req, res) => {
  try {
    const { kode_project } = req.body;
    const deleteProject = Project.destroy({
      where: {
        kode_project,
      },
    });

    if (deleteProject) {
      res.json({ statusCode: 200, message: "success" });
    }
  } catch (error) {
    console.log(error);
  }
};
