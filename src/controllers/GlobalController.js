import User from "../models/UserModel.js";
import Employee from "../models/EmployeeModel.js";
import Project from "../models/ProjectModel.js";
import Task from "../models/TaskModel.js";

export const getCounts = async (req, res) => {
  try {
    const user = await User.count();
    const employee = await Employee.count();
    const project = await Project.count();
    const task = await Task.count();
    res.json({
      statusCode: 200,
      message: "success",
      data: {
        user,
        employee,
        project,
        task,
      },
    });
  } catch (error) {
    console.log(error);
  }
};
