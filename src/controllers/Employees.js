import Employees from "../models/EmployeeModel.js";
import User from "../models/UserModel.js";
import db from "../config/db.js";

export const getEmployees = async (req, res) => {
  try {
    const employees = await Employees.findAll();
    res.json({ statusCode: 200, message: "success", data: employees });
  } catch (error) {
    console.log(error);
  }
};

export const getEmployeeXUser = async (req, res) => {
  try {
    const employees = await db.query(
      "SELECT * FROM karyawan a, user b WHERE a.no_karyawan=b.no_karyawan"
    );
    res.json({ statusCode: 200, message: "success", data: employees[0] });
  } catch (error) {
    console.log(error);
  }
};

export const getLoopEmployee = async (req, res) => {
  try {
    const { data } = req.body;
    const employeeIDData = JSON.parse(data);
    let employeeData = [];

    for (let index = 0; index < employeeIDData.length; index++) {
      const response = await Employees.findOne({
        where: {
          no_karyawan: employeeIDData[index],
        },
      });

      if (response) {
        employeeData.push(response);
      }
    }

    res.json({ statusCode: 200, message: "success", data: employeeData });
  } catch (error) {
    console.log(error);
  }
};

export const getEmployeeByID = async (req, res) => {
  try {
    const id = req.params.id;
    const employees = await Employees.findOne({
      where: { no_karyawan: id },
    });

    if (!employees) {
      return res.json({ statusCode: 404, message: "success" });
    }

    res.json({ statusCode: 200, message: "success", data: employees });
  } catch (error) {
    console.log(error);
  }
};

export const saveEmployee = async (req, res) => {
  const { no_karyawan, nama_karyawan, jenkel, jabatan, no_handphone, alamat } =
    req.body;

  try {
    const getEmployee = await Employees.findOne({
      where: { no_karyawan: no_karyawan },
    });

    if (getEmployee) {
      return res.json({
        statusCode: 400,
        message: "No Karyawan sudah ada di database !!!",
      });
    }

    await Employees.create({
      no_karyawan,
      nama_karyawan,
      jenkel,
      jabatan,
      no_handphone,
      alamat,
    });
    res.json({ statusCode: 201, message: "success simpan data karyawan" });
  } catch (error) {
    console.log(error);
  }
};

export const updateEmployee = async (req, res) => {
  const { no_karyawan, nama_karyawan, jenkel, jabatan, no_handphone, alamat } =
    req.body;

  try {
    await Employees.update(
      {
        nama_karyawan,
        jenkel,
        jabatan,
        no_handphone,
        alamat,
      },
      {
        where: {
          no_karyawan,
        },
      }
    );
    res.json({ statusCode: 201, message: "success update data karyawan" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteEmployee = async (req, res) => {
  try {
    const { no_karyawan } = req.body;
    const deleteEmployee = Employees.destroy({
      where: {
        no_karyawan,
      },
    });

    User.destroy({
      where: {
        no_karyawan,
      },
    });

    if (deleteEmployee) {
      res.json({ statusCode: 200, message: "success delete data karyawan" });
    }
  } catch (error) {
    console.log(error);
  }
};
