import User from "../models/UserModel.js";
import { v4 as uuidv4 } from "uuid";
import Employees from "../models/EmployeeModel.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import db from "../config/db.js";
import { Op } from "sequelize";

export const getUsers = async (req, res) => {
  try {
    const kode_user = req.query.kode_user;
    // const user = await User.findAll({
    //   include: [{ model: Employees }],
    // });
    let user = [];
    if (kode_user) {
      user = await db.query(
        `SELECT * FROM user a, karyawan b WHERE a.no_karyawan=b.no_karyawan AND a.kode_user!='${kode_user}'`
      );
    } else {
      user = await db.query(
        `SELECT * FROM user a, karyawan b WHERE a.no_karyawan=b.no_karyawan`
      );
    }
    res.json({ statusCode: 200, message: "success", data: user[0] });
  } catch (error) {
    console.log(error);
  }
};

export const getUserJoinEmployee = async (req, res) => {
  try {
    const user = await db.sequelize.query(
      "SELECT * FROM user a, karyawan b WHERE a.no_karyawan=b.no_karyawan",
      { type: db.sequelize.QueryTypes.SELECT }
    );
    res.json({ statusCode: 200, message: "success", data: user });
  } catch (error) {
    console.log(error);
  }
};

export const getUserByID = async (req, res) => {
  try {
    const kode_user = req.params.kode_user;
    const user = await User.findOne({
      where: { kode_user },
    });

    if (!user) {
      return res.json({ statusCode: 404, message: "success" });
    }

    res.json({ statusCode: 200, message: "success", data: user[0] });
  } catch (error) {
    console.log(error);
  }
};

export const saveUser = async (req, res) => {
  const { username, password, no_karyawan, level } = req.body;

  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt);

  try {
    const findUser = await User.findOne({
      where: {
        no_karyawan,
      },
    });

    const findUserByUsername = await User.findOne({
      where: {
        username,
      },
    });

    if (findUser)
      return res.json({
        statusCode: 304,
        message: "karyawan sudah memiliki user id",
      });

    if (findUserByUsername)
      return res.json({
        statusCode: 304,
        message: "Username sudah ada yang gunakan",
      });

    await User.create({
      kode_user: uuidv4(),
      username,
      password: hashPassword,
      no_karyawan,
      level,
    });
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const updateUserByAdmin = async (req, res) => {
  const { kode_user, username, currentUsername, level } = req.body;

  const findOtherUser = await User.findAll({
    where: {
      username: {
        [Op.not]: currentUsername,
      },
    },
  });

  const findSameUsername = findOtherUser.find(
    (val) => val.username === username
  );

  if (findSameUsername) {
    return res.json({
      statusCode: 304,
      message: "username sudah ada yang gunakan",
    });
  }

  await User.update(
    {
      username,
      level,
    },
    { where: { kode_user } }
  );

  res.json({ statusCode: 200, message: "update data user success" });
};

export const updatePassword = async (req, res) => {
  const { kode_user, password } = req.body;

  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt);

  await User.update(
    {
      password: hashPassword,
    },
    { where: { kode_user } }
  );

  res.json({ statusCode: 200, message: "change password user success" });
};

export const deleteUser = async (req, res) => {
  try {
    const { no_karyawan } = req.body;
    const deleteUser = User.destroy({
      where: {
        no_karyawan,
      },
    });

    if (deleteUser) {
      res.json({ statusCode: 200, message: "success" });
    }
  } catch (error) {
    console.log(error);
  }
};

export const Login = async (req, res) => {
  const { username, password } = req.body;

  // Check for user username
  const user = await User.findOne({ where: { username: username } });

  if (!user) {
    return res
      .status(404)
      .json({ statusCode: 404, message: "username tidak ada di database" });
  }

  if (user) {
    const match = await bcrypt.compare(password, user.password);

    if (!match) {
      return res
        .status(400)
        .json({ statusCode: 400, message: "password salah!!!" });
    }

    const employee = await Employees.findOne({
      where: { no_karyawan: user.no_karyawan },
    });

    if (match) {
      res.json({
        kode_user: user.kode_user,
        nama_karyawan: employee.nama_karyawan,
        username: user.username,
        no_karyawan: user.no_karyawan,
        level: user.level,
        token: generateToken(user.kode_user, user.username, user.no_karyawan),
      });
    } else {
      res.status(400);
      throw new Error("Invalid credentials");
    }
  }
};

const generateToken = (kode_user, username, no_karyawan) => {
  return jwt.sign(
    { kode_user, username, no_karyawan },
    process.env.ACCESS_TOKEN,
    {
      expiresIn: "1d",
    }
  );
};

export const getMe = async (req, res) => {
  try {
    const no_karyawan = req.params.no_karyawan;

    const users = await User.findOne({
      where: {
        no_karyawan,
      },
    });

    const employees = await Employees.findOne({
      where: {
        no_karyawan,
      },
    });

    if (users && employees) {
      res.json({
        statusCode: 200,
        message: "success",
        data: {
          user: users,
          employee: employees,
        },
      });
    }
  } catch (error) {
    console.log(error);
  }
};

export const Logout = async (req, res) => {
  const refToken = req.cookies.refreshToken;
  if (!refToken) return res.sendStatus(204);

  const user = User.findAll({
    where: {
      refresh_token: refToken,
    },
  });

  if (!user[0]) return res.sendStatus(204);

  const userID = user[0].kode_user;

  await user.update(
    { refresh_token: null },
    {
      where: {
        id: userID,
      },
    }
  );

  res.clearCookie("refreshToken");
  res.sendStatus(200);
};

export const updateUser = async (req, res) => {
  const {
    username,
    no_karyawan,
    nama_karyawan,
    jenkel,
    jabatan,
    no_handphone,
    alamat,
  } = req.body;

  await Employees.update(
    {
      nama_karyawan,
      jenkel,
      jabatan,
      no_handphone,
      alamat,
    },
    { where: { no_karyawan } }
  );

  await User.update(
    {
      username,
    },
    { where: { no_karyawan } }
  );

  res.json({ statusCode: 200, message: "success update profile" });
};
