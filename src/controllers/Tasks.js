import Task from "../models/TaskModel.js";
import { v4 as uuidv4 } from "uuid";
import { Op } from "sequelize";

export const getTasks = async (req, res) => {
  try {
    const task = await Task.findAll();
    res.json({ statusCode: 200, message: "success", data: task });
  } catch (error) {
    console.log(error);
  }
};

export const getTaskByBalance = async (req, res) => {
  try {
    const task = await Task.findAll({
      where: {
        balance_out: {
          [Op.not]: 0,
        },
      },
    });

    res.json({ statusCode: 200, message: "success", data: task });
  } catch (error) {
    console.log(error);
  }
};

export const getTaskByID = async (req, res) => {
  try {
    const kode_task = req.params.kode_task;
    const task = await Task.findOne({
      where: { kode_task },
    });

    if (!task) {
      return res.json({ statusCode: 404, message: "Data not found!" });
    }

    res.json({ statusCode: 200, message: "success", data: task });
  } catch (error) {
    console.log(error);
  }
};

export const changeTaskStatus = async (req, res) => {
  try {
    const { kode_task, status } = req.body;
    await Task.update({ status: status }, { where: { kode_task: kode_task } });

    res.json({ statusCode: 200, message: "Success update status task" });
  } catch (error) {
    console.log(error);
  }
};

export const getTaskByKodeProject = async (req, res) => {
  try {
    const kode_project = req.params.kode_project;
    const task = await Task.findAll({
      where: { kode_project },
    });

    if (!task) {
      return res.json({ statusCode: 404, message: "Data not found!" });
    }

    res.json({ statusCode: 200, message: "success", data: task });
  } catch (error) {
    console.log(error);
  }
};

export const saveTask = async (req, res) => {
  const { nama_task, balance_out, assign_to, kode_project, deskripsi_task } =
    req.body;

  try {
    await Task.create({
      kode_task: uuidv4(),
      nama_task,
      balance_out,
      assign_to,
      kode_project,
      deskripsi_task,
      status: 1,
    });
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const updateTask = async (req, res) => {
  const { kode_task, nama_task, balance_out, deskripsi_task } = req.body;

  try {
    await Task.update(
      {
        nama_task,
        balance_out,
        deskripsi_task,
      },
      {
        where: {
          kode_task,
        },
      }
    );
    res.json({ statusCode: 201, message: "success" });
  } catch (error) {
    console.log(error);
  }
};

export const deleteTask = async (req, res) => {
  try {
    const { kode_task } = req.body;
    const deleteTask = Task.destroy({
      where: {
        kode_task: kode_task,
      },
    });

    if (deleteTask) {
      res.json({ statusCode: 200, message: "Delete task success" });
    }
  } catch (error) {
    console.log(error);
  }
};
