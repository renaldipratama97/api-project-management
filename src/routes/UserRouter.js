import express from "express";
import {
  updateUser,
  getUsers,
  getUserJoinEmployee,
  getUserByID,
  saveUser,
  updateUserByAdmin,
  updatePassword,
  deleteUser,
  getMe,
  Login,
  Logout,
} from "../controllers/Users.js";
import { refreshToken } from "../controllers/refreshToken.js";

const router = express.Router();

router.post("/auth/login", Login);
router.delete("/auth/logout", Logout);
router.get("/user", getUsers);
router.get("/user/profile/:no_karyawan", getMe);
router.get("/user_join_employee", getUserJoinEmployee);
router.post("/user/create", saveUser);
router.post("/user/update", updateUserByAdmin);
router.post("/user/update/change_password", updatePassword);
router.post("/user/profile/update", updateUser);
router.post("/user/delete", deleteUser);
router.get("/user/:kode_user", getUserByID);
router.get("/token", refreshToken);

export default router;
