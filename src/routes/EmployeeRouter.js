import express from "express";
import {
  getEmployees,
  getEmployeeByID,
  getLoopEmployee,
  saveEmployee,
  updateEmployee,
  deleteEmployee,
  getEmployeeXUser,
} from "../controllers/Employees.js";

const router = express.Router();

router.get("/employees", getEmployees);
router.get("/employeexuser", getEmployeeXUser);
router.get("/employees/:id", getEmployeeByID);
router.post("/employee/get_employee", getLoopEmployee);
router.post("/employee/create", saveEmployee);
router.post("/employee/update", updateEmployee);
router.post("/employee/delete", deleteEmployee);

export default router;
