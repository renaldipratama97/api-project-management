import express from "express";
import {
  getTasks,
  getTaskByID,
  saveTask,
  deleteTask,
  getTaskByKodeProject,
  changeTaskStatus,
  getTaskByBalance,
  updateTask,
} from "../controllers/Tasks.js";

const router = express.Router();

router.get("/tasks", getTasks);
router.get("/tasks/kode_project/:kode_project", getTaskByKodeProject);
router.post("/tasks/create", saveTask);
router.post("/tasks/update", updateTask);
router.post("/tasks/delete", deleteTask);
router.get("/tasks/:kode_task", getTaskByID);
router.get("/tasks/bybalance/get", getTaskByBalance);
router.post("/task/change_status", changeTaskStatus);

export default router;
