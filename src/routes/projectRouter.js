import express from "express";
import {
  getProjects,
  getProjectByID,
  saveProject,
  updateProject,
  deleteProject,
  getProjectForReport,
} from "../controllers/Projects.js";

const router = express.Router();

router.get("/project", getProjects);
router.get("/project/for-report", getProjectForReport);
router.post("/project/create", saveProject);
router.post("/project/update", updateProject);
router.post("/project/delete", deleteProject);
router.get("/project/:kode_project", getProjectByID);

export default router;
