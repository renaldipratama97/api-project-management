import express from "express";
import { getCounts } from "../controllers/GlobalController.js";

const router = express.Router();

router.get("/api/get-counts", getCounts);

export default router;
