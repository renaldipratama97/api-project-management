import express from "express";
import {
  getComments,
  getCommentByKodeTask,
  submitComment,
} from "../controllers/Comments.js";

const router = express.Router();

router.get("/comments", getComments);
router.get("/comment/:kode_task", getCommentByKodeTask);
router.post("/comment/create", submitComment);

export default router;
