export default (value) => {
  return `
    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <title>Laporan Keuangan Keluar</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Reggae+One&display=swap');

        .logo {
            font-family: 'Reggae One', cursive;
        }

        .container {
            width: 500px;
            margin: 0 auto;
        }

        .header-title {
            font-size: 25px;
            width: 60%;
            margin: 0 auto;
            text-align: center;
            margin-top: 20px;
        }


        .header-subtitle {
            font-size: 15px;
            width: 100%;
            text-align: center;
        }

        table {
            width: 100%;
            margin-top: 30px;
        }

        table, thead, tbody, th, td {
            border: 1px solid rgb(166, 166, 166);
        }

        tr, td {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="logo header-title">PT Wahanakarsa Swandiri
        </div>
        <div class="header-subtitle">[ Laporan Keuangan ]</div>

        <table class="min-w-full">
            <thead class="border-b">
                <tr>
                    <th scope="col">
                        No
                    </th>
                    <th scope="col">
                        Nama Task
                    </th>
                    <th scope="col">
                        Tanggal
                    </th>
                    <th scope="col">
                        Uang Keluar
                    </th>
                </tr>
            </thead>
            <tbody>
                ${value
                  .map(
                    (val, index) => `<tr class="border-b">
                <td>${index + 1}</td>
                <td>
                    ${val.nama_task}
                </td>
                <td>
                ${val.createdAt.toLocaleDateString("en-us", {
                  weekday: "short",
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                })}
                </td>
                <td>
                ${val.balance_out.toLocaleString("en-US")}
                </td>
            </tr>`
                  )
                  .join("")}
                </tbody>
        </table>
    </div>
</body>

</html>`;
};
