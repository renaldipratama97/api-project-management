export default (value) => {
  const getCompletedTask = (data) => {
    const completed = data.filter((el) => el.status === 4);
    return completed.length;
  };

  const getPercentegeTask = (data) => {
    const task = data.length;
    const result = (getCompletedTask(data) / task) * 100;
    return result ? result : 0;
  };
  return `
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <title>Laporan Progress Keluar</title>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Reggae+One&display=swap');
    
            .logo {
                font-family: 'Reggae One', cursive;
            }
    
            .container {
                width: 550px;
                margin: 0 auto;
            }
    
            .header-title {
                font-size: 25px;
                width: 60%;
                margin: 0 auto;
                text-align: center;
                margin-top: 20px;
            }
    
    
            .header-subtitle {
                font-size: 15px;
                width: 100%;
                text-align: center;
            }
    
            table {
                width: 100%;
                margin-top: 30px;
            }
    
            table, thead, tbody, th, td {
                border: 1px solid rgb(166, 166, 166);
            }
    
            tr, td {
                text-align: center;
            }
        </style>
    </head>
    
    <body>
        <div class="container">
            <div class="logo header-title">PT Wahanakarsa Swandiri
            </div>
            <div class="header-subtitle">[ Laporan Progress Project ]</div>
    
            <table class="min-w-full">
                <thead class="border-b">
                    <tr>
                        <th scope="col">
                            No
                        </th>
                        <th scope="col">
                            Nama Project
                        </th>
                        <th scope="col">
                            Jumlah Task
                        </th>
                        <th scope="col">
                            Task Completed
                        </th>
                        <th scope="col">
                            Progress (%)
                        </th>
                    </tr>
                </thead>
                <tbody>
                ${value
                  .map(
                    (val, index) => `<tr>
                        <td>${index + 1}</td>
                        <td>
                            ${val.nama_project}
                        </td>
                        <td >
                            ${val.task.length} Task
                        </td>
                        <td >
                            ${getCompletedTask(val.task)} Task
                        </td>
                        <td >
                            ${getPercentegeTask(val.task)} %
                        </td>
                    </tr>`
                  )
                  .join("")}
                </tbody>
            </table>
        </div>
    </body>
    
    </html>`;
};
