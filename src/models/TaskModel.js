import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Tasks = db.define(
  "task",
  {
    kode_task: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    nama_task: {
      type: DataTypes.STRING(30),
    },
    kode_project: {
      type: DataTypes.STRING(64),
    },
    balance_out: {
      type: DataTypes.INTEGER(11),
    },
    assign_to: {
      type: DataTypes.TEXT,
    },
    deskripsi_task: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.INTEGER(3),
    },
  },
  {
    freezeTableName: true,
  }
);

export default Tasks;
