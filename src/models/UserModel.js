import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const User = db.define(
  "user",
  {
    kode_user: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    username: {
      type: DataTypes.STRING(20),
    },
    password: {
      type: DataTypes.STRING(64),
    },
    no_karyawan: {
      type: DataTypes.STRING(15),
    },
    level: {
      type: DataTypes.STRING(20),
    },
    picture: {
      type: DataTypes.TEXT,
    },
    refresh_token: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default User;
