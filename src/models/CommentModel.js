import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Comment = db.define(
  "comment",
  {
    kode_comment: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    kode_task: {
      type: DataTypes.STRING(64),
    },
    no_karyawan: {
      type: DataTypes.STRING(15),
    },
    deskripsi: {
      type: DataTypes.TEXT,
    },
    picture: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);
export default Comment;
