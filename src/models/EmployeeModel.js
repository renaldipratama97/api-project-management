import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Employees = db.define(
  "karyawan",
  {
    no_karyawan: {
      type: DataTypes.STRING(15),
    },
    nama_karyawan: {
      type: DataTypes.STRING(30),
    },
    jenkel: {
      type: DataTypes.STRING(8),
    },
    jabatan: {
      type: DataTypes.STRING(20),
    },
    no_handphone: {
      type: DataTypes.STRING(15),
    },
    alamat: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);

export default Employees;
