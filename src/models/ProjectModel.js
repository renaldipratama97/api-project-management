import { Sequelize } from "sequelize";
import db from "../config/db.js";

const { DataTypes } = Sequelize;

const Project = db.define(
  "project",
  {
    kode_project: {
      type: DataTypes.STRING(64),
      primaryKey: true,
    },
    nama_project: {
      type: DataTypes.STRING(35),
    },
    balance: {
      type: DataTypes.INTEGER(15),
    },
    team_leader: {
      type: DataTypes.STRING(15),
    },
    assign: {
      type: DataTypes.TEXT,
    },
    lokasi: {
      type: DataTypes.TEXT,
    },
  },
  {
    freezeTableName: true,
  }
);

export default Project;
