import jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {
  const authorize = req.headers["authorization"];
  const token = authorize && authorize.split(" ")[1];

  if (token === null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN, (err, decoded) => {
    if (err) return res.sendStatus(403);

    req.email = decoded.email;
    next();
  });
};
