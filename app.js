import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
import db from "./src/config/db.js";
import employeeRouter from "./src/routes/EmployeeRouter.js";
import userRouter from "./src/routes/UserRouter.js";
import projectRouter from "./src/routes/ProjectRouter.js";
import taskRouter from "./src/routes/TaskRouter.js";
import commentRouter from "./src/routes/CommentRouter.js";
import globalRouter from "./src/routes/GlobalRouter.js";
// import Task from "./src/models/TaskModel.js";
// import Employee from "./src/models/EmployeeModel.js";
// import User from "./src/models/UserModel.js";
// import ProjectModel from "./src/models/ProjectModel.js";
// import Comment from "./src/models/CommentModel.js";
import Tasks from "./src/models/TaskModel.js";
import Project from "./src/models/ProjectModel.js";
import pdf from "html-pdf";
import pdfTemplate from "./src/documents/index.js";
import pdfTemplateBalanceProject from "./src/documents/reportBalanceProject.js";
import pdfTemplateProgressProject from "./src/documents/reportProgressProject.js";
import { Op } from "sequelize";
import path from "path";
import { fileURLToPath } from "url";
dotenv.config();
const app = express();
const config = {
  width: "30cm",
};

try {
  db.authenticate();
  console.log("db connected");
  // Task.sync();
  // Employee.sync();
  // User.sync();
  // Project.sync();
  // Comment.sync();
} catch (error) {
  console.log(error);
}
app.use(cors());
app.use(cookieParser());
app.use(express.json());
app.use(employeeRouter);
app.use(userRouter);
app.use(projectRouter);
app.use(taskRouter);
app.use(commentRouter);
app.use(globalRouter);

app.post("/api/create-pdf-balance", async (req, res) => {
  const task = await Tasks.findAll({
    where: {
      balance_out: {
        [Op.not]: 0,
      },
    },
  });

  pdf.create(pdfTemplate(task), {}).toFile("result.pdf", (err) => {
    if (err) {
      res.send(Promise.reject());
    }

    res.send(Promise.resolve());
  });
});
app.post("/api/create-pdf-balance-project", async (req, res) => {
  let data = [];
  const task = await Tasks.findAll();
  const project = await Project.findAll();

  project.forEach((val) => {
    const tempData = task.filter(
      (item) => item.kode_project === val.kode_project
    );

    data.push({
      assign: val.assign,
      balance: val.balance,
      kode_project: val.kode_project,
      nama_project: val.nama_project,
      team_leader: val.team_leader,
      lokasi: val.lokasi,
      task: tempData,
      createdAt: val.createdAt,
      updatedAt: val.updatedAt,
    });
  });

  pdf
    .create(pdfTemplateBalanceProject(data), config)
    .toFile("result.pdf", (err) => {
      if (err) {
        res.send(Promise.reject());
      }

      res.send(Promise.resolve());
    });
});
app.post("/api/create-pdf-progress-project", async (req, res) => {
  let data = [];
  const task = await Tasks.findAll();
  const project = await Project.findAll();

  project.forEach((val) => {
    const tempData = task.filter(
      (item) => item.kode_project === val.kode_project
    );

    data.push({
      assign: val.assign,
      balance: val.balance,
      kode_project: val.kode_project,
      nama_project: val.nama_project,
      team_leader: val.team_leader,
      lokasi: val.lokasi,
      task: tempData,
      createdAt: val.createdAt,
      updatedAt: val.updatedAt,
    });
  });

  pdf
    .create(pdfTemplateProgressProject(data), {})
    .toFile("result.pdf", (err) => {
      if (err) {
        res.send(Promise.reject());
      }

      res.send(Promise.resolve());
    });
});
app.get("/api/fetch-pdf", (req, res) => {
  const __filename = fileURLToPath(import.meta.url);
  const __dirname = path.dirname(__filename);
  res.sendFile(`${path.join(__dirname)}/result.pdf`);
});

app.listen(1000, () => console.log("server running in port 1000"));
